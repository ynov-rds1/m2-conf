# init script for Vagrant VMs

# update OS
dnf update -y

# install ansible
dnf install -y ansible

# désactive SELinux
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config

adduser vagrant 
usermod -aG wheel vagrant 
echo vagrant ALL=NOPASSWD:ALL > /etc/sudoers.d/vagrant

chmod 700 /home/vagrant/.ssh/
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAPuWoOc/7YcHpa7FCBUeOKBBokKK5IJfM7VqqKetOFYUnCAKuhbWPlYIPpPvCDm6UO9iVogRo+kO5MFuy3/fnSoOrGbhCz6nTfaGdp/Ombn65VwW0dtYR4qqDGbV0xci/aEY/pk2vBzh8un7SRivsRZp5k7mZGjnnpyFl0JF2sqHsbxScviqFG8/Oa7cIoLm5lK+LBdCSZthMsDF+s7uNu4fr4slRhh/TzTJqo1cGeXJJSidTGfNyl+0rl68gOZBcZ7W17e1d6ztNt2cS3jwSgqeeG+anD2YL773nlT2KRnlwWbU2htg+wQAwPJinGcYTZ4w1hfpjxdiQe/LDGeB+lt7hz8l7MJu6Ajl0hntbsIdYy80MMb/vXmP7fqSE0UezFT1IRNsGBhSwSzUieMA74rhWK5xFDiiRwFyxPluyuNRVAam4Nu2LRzfE5WJrVUEgeVoNirsIM9pREkOGip1rOrf40rdC+RxpBKbIt7h/kaasCBjpqD+gkyI3oBnfF/602yKuuo99qBpDxAqksnZE1NsfwZZ3277Jn6BXs07WJnrw9pC9GcANa/rEXF4pmCVN3I5KkEtZlm9cxMzGmiYhA03oTdH/RUCsv3n/BpEidU4ec1swr1jPWL1sxSvT1Qd76plmeBLtvnui4502GmLvwPPcBxbYZGFz5wx+Qs/giQ== sandd@LAPTOP-VRSK953C" >> /home/vagrant/.ssh/authorized_keys