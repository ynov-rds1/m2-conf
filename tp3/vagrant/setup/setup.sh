# init script for Vagrant VMs

# update OS
dnf update -y

# install ansible
dnf install -y ansible

# désactive SELinux
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config

adduser vagrant 
usermod -aG wheel vagrant 
echo vagrant ALL=NOPASSWD:ALL > /etc/sudoers.d/vagrant

chmod 700 /home/vagrant/.ssh/
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC58iWvQx6h7eyq1InAAnAzLpm/j2HOSqWIXFNMXB/GqH9wiYYoVQkLqBiEpo5rFgF1SPldjN4vBM9ta2lFvT7tSEzfaOgqO9uk9ux+mycD8sfC+8LlM332JKau08zNNUdRX4XGLgxBfxgBraTiL336zC8x4g+fVYuOJbdvf7ubZ7M24bOsAZdjdNLCNhU5X/qhRebHEPxF5cShpiR9aOvbBaOGzDPOGjMAehIPf0j1p3nr5mkDmGnadHdsy5AjJ2+/aFLlBC9da1s3alhtDVmMuHrniSekzd+8jyAH6uwn+YimYx0n+auLar7mrXOcEVdW7dPijguVBfsAtMlRCBO4tvdijokDUwIlVZ0J+aNpSyD5qHbTzqTDrI5F27z9+BfC68OnY5cBMIKEoWXNcuX3TlA2V63/YbszSnN01ngBYDZTMryFDvyEYEOgJRIu1MYmetDjOCyiwhFXypf8Tw2oNDLMNy/PPiCsCTYPIBqFjkwglRW3xadOO4DO0p8ZCTy/yi64MnakfsZeWPy+eUTYYFy16EKieAGe+WBBFv2Q+4lhH+J2M2M2Hh0Wq1l58XuIcToRj86VoNS1BhFyBEiSD4D+yr3eaM/xECgkmOdpI/p9lKVaD9CjqMGIZryvnHAOn48H6MZPVmdAByCDeQeqdKrMxpE0ht70YyWa18jkyw== sandd@LAPTOP-VRSK953C" >> /home/vagrant/.ssh/authorized_keys