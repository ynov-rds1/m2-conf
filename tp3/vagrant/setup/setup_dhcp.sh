mv /home/vagrant/dhcpd.conf /etc/dhcp/dhcpd.conf
dnf -y install dhcp-server
dnf -y install tftp-server
systemctl enable --now tftp.socket
firewall-cmd --add-service=tftp
firewall-cmd --runtime-to-permanent
systemctl restart dhcpd