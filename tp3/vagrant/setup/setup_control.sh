# install qemu-img
dnf install -y qemu-img
# install genisoimage
dnf install -y genisoimage

chmod 600 /home/vagrant/.ssh/id_rsa
chmod 644 /home/vagrant/.ssh/id_rsa.pub

# Cloud init iso creation part
# chmod 777 /home/vagrant/Rocky-9-GenericCloud-Base.latest.x86_64.qcow2

# qemu-img  convert -f qcow2 -O vdi Rocky-9-GenericCloud-Base.latest.x86_64.qcow2 /iso/rocky9.vdi
# genisoimage -output /iso/cloud-init.iso -volid cidata -joliet -r meta-data user-data